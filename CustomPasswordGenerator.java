/**
 * Copyright (c) 2014 Janua. All Rights Reserved
 * Author : Daly Chikhaoui <dchikhaoui@janua.fr>
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at legal-notices/CDDLv1_0.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 */

package com.sun.identity.password.plugins;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Properties;

import com.sun.identity.idm.AMIdentity;
import com.sun.identity.password.ui.model.PWResetException;

/**
 * <code>CustomPasswordGenerator</code> defines a set of methods
 * that are required to generate a new password for a user.
 */
public class CustomPasswordGenerator implements PasswordGenerator {
	
	private Properties configFile;
	private static SecureRandom rnd = new SecureRandom();
	private static final int PASSWORD_LENGTH = 8;
	private int length = PASSWORD_LENGTH;
	private int maxLength;
	private String forbiddenChars;
	private ArrayList<String> classes = new ArrayList<String>();
	private ArrayList<Integer> classePoints = new ArrayList<Integer>();

    /**
     * Constructs a random password generator object.
     */
    public CustomPasswordGenerator() {
        intialize();
    }
    
    /**
     * Initializes password complexity options from the configuration file
     */	
    private void intialize() {
        configFile = new Properties();
        try {
        	
        	configFile.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("ppm.conf"));

			String tmpLength = configFile.getProperty("length");
			if (tmpLength != null && !tmpLength.isEmpty() && !tmpLength.equals("0"))
				length = Integer.parseInt(tmpLength);
			
			maxLength = Integer.parseInt(configFile.getProperty("maxLength"));
			
			length = (length>maxLength && maxLength!=0)?maxLength:length;

			forbiddenChars = configFile.getProperty("forbiddenChars");
			
			for (String key : configFile.stringPropertyNames()) {
				if (key.toLowerCase().startsWith("class-")) {
					classes.add(configFile.getProperty(key));
					classePoints.add(0);
				}
			}
				
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Generates new password for user.
     *
     * @param user User object.
     * @return new password for user.
     * @throws PWResetException if password cannot be generated.
     */
	public String generatePassword(AMIdentity user) throws PWResetException {
	    	
	    	String genPwd = "";
	    	boolean firstLoop = true;
	    	
	    	// Parcourir chaque classe et en prendre un caractère
	    	// Les classes obligatoires sont prioritaires
	     	while (genPwd.length() < length) {
		    	for (int i=0 ; i<classes.size(); i++)
		    	{
		    		String [] classeTab = classes.get(i).split(" ");
		    		String classe = classeTab[0];
		    		int classeMin = Integer.parseInt(classeTab[1]);
		    		int classeMinForPoint = Integer.parseInt(classeTab[2]);
		    		
		    		// A la première boucle DO, si classe non nécessaire passer à la suivante 
		    		if (firstLoop && classeMin==0)
		    			continue;
		    		
		    		int nbChars = 1;
	
		        	// A la première boucle on prend suffisamment caractères pour "marquer" un point
		    		if (firstLoop)
		    			nbChars = (classeMin>classeMinForPoint)?classeMin:classeMinForPoint;
		    		
		    		char tmpChar;
		    		for (int j=0; j<nbChars && (genPwd.length() < length); j++) {
		    			do {
		    				tmpChar = classe.charAt(rnd.nextInt(classe.length()));
		    			}
		    			while (forbiddenChars.contains(tmpChar+""));
		    			genPwd += tmpChar;
		    		}
		    		
		    		classePoints.set(i, 1);
		    	}
		    	
		    	firstLoop = false;
	    	}
	    	
	        return genPwd;
	    }
	}
